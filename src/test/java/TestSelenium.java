import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TestSelenium {
    @Test
    public void testSelenium() throws Exception {
        // создаем новый экземпляр html unit driver
        // Обратите внимание, что последующий код не закладывается на
        // конкретную, имплементацию, а только на интерфейс WebDriver.
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);
        WebDriver driver = new FirefoxDriver(capabilities);

        // Открываем Google
        driver.get("http://www.google.com");

        // Находим по имени поле для ввода
        WebElement element = driver.findElement(By.xpath("//*[@id=\'lst-ib\']"));

        // Вводим ключевое слово для поиска
        element.sendKeys("selenium");

        // Отправляем форму в которой находится элемент element.
        // WebDriver сам найдет, в какой он форме.
        element.submit();

        Thread.sleep(1000);

        // Выводим в консоль заголовок страницы
        System.out.println("Page title is: " + driver.getTitle());
        Assert.assertTrue(driver.getTitle().equals("selenium - Поиск в Google"));
        driver.quit();
    }
}
